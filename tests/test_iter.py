"""Test iteration helpers."""
from typing import Union

import pytest

from libadvian.iter import chunked


@pytest.mark.parametrize("data", (bytearray((8, 16, 32, 64, 96, 12)), memoryview(bytearray((8, 16, 32, 64, 96, 12)))))
def test_chunked(data: Union[bytearray, memoryview]) -> None:
    """Test the chunker"""
    cnt = 0
    for chunk in chunked(data, 3):
        assert len(chunk) == 3
        if cnt == 0:
            assert chunk == (8, 16, 32)
        if cnt == 1:
            assert chunk == (64, 96, 12)
        cnt += 1
