"""Test the test-helpers module"""
from typing import Generator
from pathlib import Path
import platform
import socket
import logging
import uuid

import pytest


LOGGER = logging.getLogger(__name__)

# pylint: disable=W0621


@pytest.mark.skipif(platform.system() == "Windows", reason="can't use IPC sockets on windows")
def test_nice_tmpdir_ipc(nice_tmpdir: str) -> None:
    """Check that we can create sockets in the tempdir"""
    path = Path(nice_tmpdir) / Path(f"some_random_{uuid.uuid4()}_ipc.sock")
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    LOGGER.debug("binding {} to {}".format(sock, path))
    sock.bind(str(path).encode("utf-8"))
    sock.close()


def test_nice_tmpdir_writable(nice_tmpdir: str) -> None:
    """Make sure it exists and we can write to it"""
    dirpath = Path(nice_tmpdir)
    assert dirpath.exists()
    filepath = dirpath / Path("foo.txt")
    with filepath.open("wt", encoding="utf-8") as fpntr:
        fpntr.write("Hääyöaie\n")


@pytest.fixture(scope="module")
def fxt_scoped_tmpdir_mod(nice_tmpdir_mod: str, monkeymodule: pytest.MonkeyPatch) -> Generator[str, None, None]:
    """test inheriting fixtures and scopes"""
    dirstr = str(nice_tmpdir_mod)
    with monkeymodule.context() as mpatch:
        mpatch.setenv("MPATCH_MODULE", "1")
        yield dirstr


@pytest.fixture(scope="session")
def fxt_scoped_tmpdir_ses(nice_tmpdir_ses: str, monkeysession: pytest.MonkeyPatch) -> Generator[str, None, None]:
    """test inheriting fixtures and scopes"""
    dirstr = str(nice_tmpdir_ses)
    with monkeysession.context() as mpatch:
        mpatch.setenv("MPATCH_SESSION", "1")
        yield dirstr


def test_scoped_tmpdirs(fxt_scoped_tmpdir_mod: str, fxt_scoped_tmpdir_ses: str) -> None:
    """Test the scope inherited directories are writable"""
    for dirstr in (fxt_scoped_tmpdir_mod, fxt_scoped_tmpdir_ses):
        dirpath = Path(dirstr)
        assert dirpath.exists()
        filepath = dirpath / Path("bar.txt")
        with filepath.open("wt", encoding="utf-8") as fpntr:
            fpntr.write("Jännevälitarkastaja\n")
