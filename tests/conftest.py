"""Automagical fixtures"""
import logging

import pytest

from libadvian.logging import init_logging

# pylint: disable=W0611 ; # We want these available to other tests
from libadvian.testhelpers import (
    nice_tmpdir,
    nice_tmpdir_mod,
    nice_tmpdir_ses,
    monkeysession,
    monkeymodule,
)


@pytest.fixture(autouse=True)
def stdlogging() -> None:
    """Init debug logging in our standard format"""
    init_logging(logging.DEBUG)
