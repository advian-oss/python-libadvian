"""Test the hashing helpers"""
from typing import Any, List, Union, Dict
import logging
import json
from dataclasses import dataclass, field

from frozendict import frozendict
import pytest

from libadvian.hashinghelpers import immobilize, IMTypes, ImmobilizeError, FrozendictEncoder

LOGGER = logging.getLogger(__name__)


@dataclass
class MutableClass:
    """Mutable dummy class"""

    prop: str = field(default="foo")


@dataclass(frozen=True)
class ImmutableClass:
    """Mutable dummy class"""

    prop: str = field(default="foo")


@pytest.mark.parametrize("data_in", ["foo", 10, 11.1, True, False, None, ImmutableClass()])
def test_basic(data_in: IMTypes) -> None:
    """Test the basic types"""
    assert data_in == immobilize(data_in)


def test_list() -> None:
    """Test a of valid values gets converted to tuple of the same"""
    lst: List[Union[str, int, float, bool, None]] = ["foo", 10, 11.1, True, False, None]
    tpl = immobilize(lst)
    assert isinstance(tpl, tuple)
    for idx, val in enumerate(tpl):
        assert lst[idx] == val


def test_dict() -> None:
    """Test that a dictionary is converted correctly"""
    dct: Dict[Union[str, int, float], Union[str, int, float, bool]] = {
        "foo": "bar",
        10: "number",
        0.2: 10,
    }
    fdct = immobilize(dct)
    assert isinstance(fdct, frozendict)
    for key, value in fdct.items():
        assert value == dct[key]
    # Check that JSON encoding works
    encoded = json.dumps(fdct, cls=FrozendictEncoder)
    decoded = json.loads(encoded)
    assert isinstance(decoded, dict)
    assert decoded["foo"] == "bar"


@pytest.mark.parametrize("data_in", [MutableClass()])
def test_invalid(data_in: Any) -> None:
    """Test invalid inputs"""
    with pytest.raises(ImmobilizeError):
        _ = immobilize(data_in)


@pytest.mark.parametrize("data_in", [MutableClass()])
def test_invalid_ignore(data_in: Any) -> None:
    """Test invalid inputs with ignore enabled"""
    assert immobilize(data_in, True) is None


def test_dict_recurse() -> None:
    """Test dict recursing"""
    subdct: Dict[Union[str, int, float], Union[str, int, float, bool]] = {
        "foo": "bar",
        10: "number",
        0.2: 10,
    }
    sublst: List[Union[str, int, float, bool, None]] = ["foo", 10, 11.1, True, False, None]
    ret = immobilize(
        {
            "dict": subdct,
            "list": sublst,
        }
    )
    assert isinstance(ret, frozendict)
    assert isinstance(ret["dict"], frozendict)
    assert isinstance(ret["list"], tuple)
    for key, value in ret["dict"].items():
        assert value == subdct[key]
    for idx, val in enumerate(ret["list"]):
        assert sublst[idx] == val

    # Check that JSON encoding works
    encoded = json.dumps(ret, cls=FrozendictEncoder)
    decoded = json.loads(encoded)
    assert isinstance(decoded, dict)
    assert isinstance(decoded["list"], list)
    assert isinstance(decoded["dict"], dict)
    assert decoded["dict"]["foo"] == "bar"
    assert decoded["list"][1] == 10


def test_list_recurse() -> None:
    """Test list recursing"""
    subdct: Dict[Union[str, int, float], Union[str, int, float, bool]] = {
        "foo": "bar",
        10: "number",
        0.2: 10,
    }
    sublst: List[Union[str, int, float, bool, None]] = ["foo", 10, 11.1, True, False, None]
    ret = immobilize([subdct, sublst])
    assert isinstance(ret, tuple)
    assert isinstance(ret[0], frozendict)
    assert isinstance(ret[1], tuple)
    for key, value in ret[0].items():
        assert value == subdct[key]
    for idx, val in enumerate(ret[1]):
        assert sublst[idx] == val

    # Check that JSON encoding works
    encoded = json.dumps(ret, cls=FrozendictEncoder)
    decoded = json.loads(encoded)
    assert isinstance(decoded, list)
    assert isinstance(decoded[0], dict)
    assert isinstance(decoded[1], list)
    assert decoded[0]["foo"] == "bar"
    assert decoded[1][1] == 10
