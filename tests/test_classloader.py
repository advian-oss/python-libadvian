"""Test the class loader"""
from libadvian.classloader import get_class, get_instance
from libadvian.logging import UTCISOFormatter


def test_get_class() -> None:
    """Test getting a class"""
    klass = get_class("libadvian.logging.UTCISOFormatter")
    assert klass == UTCISOFormatter


def test_get_instance_args() -> None:
    """Test getting a class"""
    instance = get_instance("libadvian.logging.UTCISOFormatter", None, None)
    assert isinstance(instance, UTCISOFormatter)


def test_get_instance_kwargs() -> None:
    """Test getting a class"""
    instance = get_instance("libadvian.logging.UTCISOFormatter", fmt=None, datefmt=None)
    assert isinstance(instance, UTCISOFormatter)
