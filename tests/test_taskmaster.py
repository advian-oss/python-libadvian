"""Test the taskmaster"""
import logging
import asyncio

import pytest

from libadvian.tasks import TaskMaster

LOGGER = logging.getLogger(__name__)


@pytest.mark.asyncio
async def test_fire_and_forget() -> None:
    """Test that we can tasks"""

    async def log_sleep() -> None:
        """log msg and sleep"""
        LOGGER.info("Going to sleep")
        await asyncio.sleep(1.0)
        LOGGER.info("Woke up and ending")

    task = TaskMaster.singleton().create_task(log_sleep())
    assert task.get_name() in TaskMaster.singleton()._tasks  # pylint: disable=W0212
    await asyncio.sleep(1.2)
    assert task.done()
    assert task.get_name() not in TaskMaster.singleton()._tasks  # pylint: disable=W0212


@pytest.mark.asyncio
async def test_stop() -> None:
    """Test stopping tasks"""

    async def forever_sleep() -> None:
        """loop sleep"""
        while True:
            LOGGER.info("Going to sleep")
            await asyncio.sleep(1.0)
            LOGGER.info("Woke up and looping")

    task = TaskMaster.singleton().create_task(forever_sleep(), name="forever")
    await asyncio.sleep(1.2)
    assert not task.done()
    await TaskMaster.singleton().stop_named_task_graceful("forever")
    assert task.done()

    for cnt in range(3):
        TaskMaster.singleton().create_task(forever_sleep(), name=f"forever-{cnt}")
    await asyncio.sleep(0.25)
    await TaskMaster.singleton().stop_lingering_tasks()
    for cnt in range(3):
        assert f"forever-{cnt}" not in TaskMaster.singleton()._tasks  # pylint: disable=W0212
