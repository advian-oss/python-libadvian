"""Test the HTTPMultiRecordHandler and related logging parts"""
import logging
import logging.config
import os
import copy

import pytest
import requests

from libadvian.logging.common import AddExtrasFilter
from libadvian.logging.httpmulti import HTTPMultiRecordHandler, BufferedHTTPMultiRecordHandler, HTTP_LOGGING_CONFIG
from libadvian.testhelpers import echo_http_server, EchoServerYieldType  # pylint: disable=W0611

LOGGER = logging.getLogger(__name__)


# pylint: disable=W0621


@pytest.mark.parametrize("send_data", ["Single line", "Two\nlines"])
def test_echo_server(echo_http_server: EchoServerYieldType, send_data: str) -> None:
    """Test the echo server"""
    uri, squeue = echo_http_server
    resp = requests.post(uri, data=send_data, timeout=1.0)
    LOGGER.debug("{} content: {!r}".format(resp, resp.content))
    assert resp.content == send_data.encode("utf-8")
    item = squeue.get(timeout=1.0)
    assert item["reply_body"] == send_data.encode("utf-8")


def test_logging_handler_unbuffered(echo_http_server: EchoServerYieldType) -> None:
    """create a logger and override the handler to test the unbuffered http"""
    vlog = logging.getLogger("httptest")
    uri, squeue = echo_http_server
    handler = HTTPMultiRecordHandler(uri)
    vlog.addHandler(handler)
    vlog.debug("Hello world! from debug")
    item = squeue.get(timeout=1.0)
    assert b"[DEBUG]" in item["reply_body"]
    vlog.info("Hello world! from info\n  with multiple lines")
    item = squeue.get(timeout=1.0)
    assert b"[INFO]" in item["reply_body"]
    vlog.warning("Hello world! from warning")
    item = squeue.get(timeout=1.0)
    assert b"[WARNING]" in item["reply_body"]
    vlog.error("Hello world! from error")
    item = squeue.get(timeout=1.0)
    assert b"[ERROR]" in item["reply_body"]
    handler.flush()
    handler.close()


def test_logging_handler_multilevel_extras(echo_http_server: EchoServerYieldType) -> None:
    """create a logger and override the handler to test the unbuffered http"""
    vlog = logging.getLogger("httptest")
    uri, squeue = echo_http_server
    handler = HTTPMultiRecordHandler(uri)
    vlog.addHandler(handler)
    vlog.debug("Extra with list", extra={"list_of_stuff": [10, 9, 8]})
    item = squeue.get(timeout=1.0)
    assert "labels_json" in item["headers"]
    assert "list_of_stuff" in item["headers"]["labels_json"]
    vlog.debug("Extra with dict", extra={"dict_of_stuff": {"item": "value", "list": ["one", "two"], 10: 0.5}})
    item = squeue.get(timeout=1.0)
    assert "labels_json" in item["headers"]
    assert "dict_of_stuff" in item["headers"]["labels_json"]


def test_logging_handler_buffered(echo_http_server: EchoServerYieldType) -> None:
    """create a logger and override the handler to test buffered http"""
    vlog = logging.getLogger("bufftest")
    uri, squeue = echo_http_server
    handler = BufferedHTTPMultiRecordHandler(uri)
    # kill the background auto-flush
    handler._interval_should_stop = True  # pylint: disable=W0212
    vlog.addHandler(handler)
    vlog.debug("Hello world! from debug")
    vlog.info("Hello world! from info\n  with multiple lines")
    vlog.warning("Hello world! from warning, with exra labels", extra={"testlabel": "testvalue"})
    vlog.error("Hello world! from error")  # this will flush the buffers to a POST
    handler.close()
    # Check the request values the server saw, actual execution order of the posts might be undetermined
    item1 = squeue.get(timeout=1.0)
    item2 = squeue.get(timeout=1.0)
    if "labels_json" in item1["headers"]:
        wextras = item1
        noextras = item2
    else:
        wextras = item2
        noextras = item1
    assert "labels_json" not in noextras["headers"]
    assert "labels_json" in wextras["headers"]
    assert b"from warning, with exra labels" in wextras["request_body"]
    assert b"from info\\n  with multiple lines" in noextras["request_body"]


def test_logging_handler_dictconfig_wfilter(echo_http_server: EchoServerYieldType) -> None:
    """Test that we can use the http dictconfig via ENV uri"""
    uri, squeue = echo_http_server
    os.environ["LOG_HTTP_TARGET_URI"] = uri
    config = copy.deepcopy(HTTP_LOGGING_CONFIG)
    config["filters"] = {
        "global_labels": {
            "()": AddExtrasFilter,
            "extras": {"globalflag1": "testvalue"},
        },
    }
    for key in config["handlers"]:
        if "filters" not in config["handlers"][key]:
            config["handlers"][key]["filters"] = []
        config["handlers"][key]["filters"].append("global_labels")
    config["root"]["level"] = logging.DEBUG
    logging.config.dictConfig(config)

    # Remove the http handler
    handler = None
    root = logging.getLogger()
    for hdl in root.handlers:
        if isinstance(hdl, BufferedHTTPMultiRecordHandler):
            handler = hdl
            break
    if not handler:
        raise RuntimeError("could not find correct handler")
    # kill the background auto-flush
    handler._interval_should_stop = True  # pylint: disable=W0212

    vlog = logging.getLogger("confdicttest")
    vlog.debug("Hello world! from debug")
    vlog.info("Hello world! from info\n  with multiple lines")
    vlog.warning("Hello world! from warning, with exra labels", extra={"testlabel": "testvalue"})
    vlog.error("Hello world! from error")  # this will flush the buffers to a POST
    handler.wait_inflight()
    handler.close()
    # Stop polluting the root logger with the http one
    root.removeHandler(handler)
    del os.environ["LOG_HTTP_TARGET_URI"]

    # Check what we got from server
    item1 = squeue.get(timeout=1.0)
    item2 = squeue.get(timeout=1.0)
    assert "labels_json" in item1["headers"]
    assert "labels_json" in item2["headers"]
    assert "globalflag1" in item1["headers"]["labels_json"]
    assert "globalflag1" in item2["headers"]["labels_json"]
    if "testlabel" in item1["headers"]["labels_json"]:
        wextras = item1
        noextras = item2
    else:
        wextras = item2
        noextras = item1
    assert "testlabel" in wextras["headers"]["labels_json"]
    assert "testlabel" not in noextras["headers"]["labels_json"]
    assert b"from warning, with exra labels" in wextras["request_body"]
    assert b"from info\\n  with multiple lines" in noextras["request_body"]
